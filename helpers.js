function getOffsetLeft(elem) {
    var offsetLeft = 0;
    do {
        if (!isNaN(elem.offsetLeft)) {
            offsetLeft += elem.offsetLeft;
        }
    } while (elem = elem.offsetParent);
    return offsetLeft;
}

function getOffsetTop(elem) {
    var offsetLeft = 0;
    do {
        if (!isNaN(elem.offsetTop)) {
            offsetLeft += elem.offsetTop;
        }
    } while (elem = elem.offsetParent);
    return offsetLeft;
}

function getComputedStyles(element) {
    return window.getComputedStyle(element);
}

function moveUpFixed(element, level) {
    level = level || 0;

    var style = getComputedStyles(element);
    if (style.position == "fixed" || (level == 1 && style.position == "absolute")) {
        if (style.top) {

            element.style.top = (parseInt(getComputedStyles(element).top) - 100) + 'px'
        }

    } else if (element.children)
        for (var i = 0; i < element.children.length; i++) {
            moveUpFixed(element.children[i], level + 1)
        }
}

function moveDownFixed(element, level) {
    level = level || 0;

    var style = getComputedStyles(element);
    if (style.position == "fixed" || (level == 1 && style.position == "absolute")) {
        if (style.top) {

            element.style.top = (parseInt(getComputedStyles(element).top) + 100) + 'px'
        }

    } else if (element.children)
        for (var i = 0; i < element.children.length; i++) {
            moveDownFixed(element.children[i], level + 1)
        }
}

//simplify complex url to just the path
function cutUrl(url){
    let fromIndex = url.lastIndexOf('/');
    let cut = url.slice(0, fromIndex);
    //console.log('cutUrl: index: ' + fromIndex + ' cut: ' + cut);
    return cut;
}

var colors = [{
        'r': 235,
        'g': 75,
        'b': 0
    },
    {
        'r': 225,
        'g': 125,
        'b': 255
    },
    {
        'r': 180,
        'g': 7,
        'b': 20
    },
    {
        'r': 80,
        'g': 170,
        'b': 240
    },
    {
        'r': 180,
        'g': 90,
        'b': 135
    },
    {
        'r': 195,
        'g': 240,
        'b': 0
    },
    {
        'r': 150,
        'g': 18,
        'b': 255
    },
    {
        'r': 80,
        'g': 245,
        'b': 0
    },
    {
        'r': 165,
        'g': 25,
        'b': 0
    },
    {
        'r': 80,
        'g': 145,
        'b': 0
    },
    {
        'r': 80,
        'g': 170,
        'b': 240
    },
    {
        'r': 55,
        'g': 92,
        'b': 255
    },
];

function getColor(index, alpha) {
    index = index % colors.length;

    var color = colors[index];
    return "rgba(" + color.r + "," + color.g + "," + color.b + "," + alpha + ")"

}